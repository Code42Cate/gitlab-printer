# Gitlab Issue Printer


```txt
                /|  /|  ----------------------------------
                ||__||  |   You got this my friend   |
               /   O O\__                            |
              /          \                           |
             /      \     \                          |
            /   _    \     \ -------------------------
           /    |\____\     \      ||
          /     | | | |\____/      ||
         /       \| | | |/ |     __||
        /  /  \   -------  |_____| ||
       /   |   |           |       --|
       |   |   |           |_____  --|
       |  |_|_|_|          |     \----
       /\                  |
      / /\        |        /
     / /  |       |       |
 ___/ /   |       |       |
|____/    c_c_c_C/ \C_c_c_c
```

## Running

Create `.env` file with:
```
GITLAB_ACCESS_TOKEN=
GITLAB_PROJECT_ID=
```

Install go dependencies and run:

```bash
go run ./main.go
```
