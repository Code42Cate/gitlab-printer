package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	for {
		time.Sleep(20 * time.Second)

		issues, err := FetchGitlabIssues()
		if err != nil {
			log.Fatal(err)
		}

		for _, issue := range issues {

			filePath := fmt.Sprintf("./data/%d.txt", issue.ID)
			// check if file exists, if yes, skip
			if _, err := os.Stat(filePath); err == nil {
				log.Printf("Issue %d already exists, skipping\n", issue.ID)
				continue
			}

			log.Printf("Issue %d: %s\n", issue.ID, issue.Title)
			// Create a text file and write issue details
			file, err := os.Create(fmt.Sprintf("./data/%d.txt", issue.ID))
			if err != nil {
				log.Fatal(err)
			}
			defer file.Close()

			file.WriteString(generateCustomASCIIArt(issue.Title) + "\n\n")

			// Write issue details
			file.WriteString(fmt.Sprintf("Issue #%d: %s\n\n", issue.ID, issue.Title))
			file.WriteString("Author: " + issue.Author.Username + "\n")
			file.WriteString("Assigned: " + issue.Assignee.Username + "\n")
			file.WriteString("Created at: " + issue.CreatedAt.Format(time.RFC1123) + "\n")
			file.WriteString("State: " + issue.State + "\n")

			if len(issue.Labels) > 0 {
				file.WriteString("Labels: " + strings.Join(issue.Labels, ", "))
			}

			// Write additional information
			file.WriteString("\nDescription:\n")
			file.WriteString(issue.Description + "\n")

			// Print the file
			if err := exec.Command("lpr", filePath).Run(); err != nil {
				log.Fatal(err)
			}

		}
	}
}

func generateCustomASCIIArt(customText string) string {

	maxWidth := 22
	// truncate or pad custom text
	if len(customText) > maxWidth*4 {
		customText = customText[:maxWidth*4]
	} else {
		customText = customText + strings.Repeat(" ", maxWidth*4-len(customText))
	}

	// split into lines
	lines := make([]string, 4)
	for i := 0; i < 4; i++ {
		lines[i] = customText[i*maxWidth:(i+1)*maxWidth] + "|"
	}

	// pad with whitespace
	for i := 0; i < 4; i++ {
		if len(lines[i]) < maxWidth {
			lines[i] = lines[i] + strings.Repeat(" ", maxWidth-len(lines[i]))
		}
	}

	asciiArt := `
                /|  /|  ---------------------------
                ||__||  |   %s
               /   O O\__   %s
              /          \  %s
             /      \     \ %s
            /   _    \     \ ----------------------
           /    |\____\     \      ||
          /     | | | |\____/      ||
         /       \| | | |/ |     __||
        /  /  \   -------  |_____| ||
       /   |   |           |       --|
       |   |   |           |_____  --|
       |  |_|_|_|          |     \----
       /\                  |
      / /\        |        /
     / /  |       |       |
 ___/ /   |       |       |
|____/    c_c_c_C/ \C_c_c_c
`

	return fmt.Sprintf(asciiArt, lines[0], lines[1], lines[2], lines[3])
}

type GitlabIssue struct {
	ID          int       `json:"id"`
	Iid         int       `json:"iid"`
	ProjectID   int       `json:"project_id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	State       string    `json:"state"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	ClosedAt    any       `json:"closed_at"`
	ClosedBy    any       `json:"closed_by"`
	Labels      []string  `json:"labels"`
	Milestone   any       `json:"milestone"`
	Assignees   []struct {
		ID        int    `json:"id"`
		Username  string `json:"username"`
		Name      string `json:"name"`
		State     string `json:"state"`
		Locked    bool   `json:"locked"`
		AvatarURL string `json:"avatar_url"`
		WebURL    string `json:"web_url"`
	} `json:"assignees"`
	Author struct {
		ID        int    `json:"id"`
		Username  string `json:"username"`
		Name      string `json:"name"`
		State     string `json:"state"`
		Locked    bool   `json:"locked"`
		AvatarURL string `json:"avatar_url"`
		WebURL    string `json:"web_url"`
	} `json:"author"`
	Type     string `json:"type"`
	Assignee struct {
		ID        int    `json:"id"`
		Username  string `json:"username"`
		Name      string `json:"name"`
		State     string `json:"state"`
		Locked    bool   `json:"locked"`
		AvatarURL string `json:"avatar_url"`
		WebURL    string `json:"web_url"`
	} `json:"assignee"`
	UserNotesCount     int    `json:"user_notes_count"`
	MergeRequestsCount int    `json:"merge_requests_count"`
	Upvotes            int    `json:"upvotes"`
	Downvotes          int    `json:"downvotes"`
	DueDate            any    `json:"due_date"`
	Confidential       bool   `json:"confidential"`
	DiscussionLocked   any    `json:"discussion_locked"`
	IssueType          string `json:"issue_type"`
	WebURL             string `json:"web_url"`
	TimeStats          struct {
		TimeEstimate        int `json:"time_estimate"`
		TotalTimeSpent      int `json:"total_time_spent"`
		HumanTimeEstimate   any `json:"human_time_estimate"`
		HumanTotalTimeSpent any `json:"human_total_time_spent"`
	} `json:"time_stats"`
	TaskCompletionStatus struct {
		Count          int `json:"count"`
		CompletedCount int `json:"completed_count"`
	} `json:"task_completion_status"`
	BlockingIssuesCount int    `json:"blocking_issues_count"`
	HasTasks            bool   `json:"has_tasks"`
	TaskStatus          string `json:"task_status"`
	Links               struct {
		Self                string `json:"self"`
		Notes               string `json:"notes"`
		AwardEmoji          string `json:"award_emoji"`
		Project             string `json:"project"`
		ClosedAsDuplicateOf any    `json:"closed_as_duplicate_of"`
	} `json:"_links"`
	References struct {
		Short    string `json:"short"`
		Relative string `json:"relative"`
		Full     string `json:"full"`
	} `json:"references"`
	Severity           string `json:"severity"`
	MovedToID          any    `json:"moved_to_id"`
	ServiceDeskReplyTo any    `json:"service_desk_reply_to"`
}

func FetchGitlabIssues() ([]GitlabIssue, error) {
	// Replace these values with your GitLab settings
	apiURL := "https://gitlab.com/api/v4"
	projectID := os.Getenv("GITLAB_PROJECT_ID")
	privateToken := os.Getenv("GITLAB_ACCESS_TOKEN")
	// Create the API endpoint URL for getting issues
	issuesURL := fmt.Sprintf("%s/projects/%s/issues?state=opened", apiURL, projectID)

	// Create an HTTP client
	client := &http.Client{}

	// Create the HTTP request
	req, err := http.NewRequest("GET", issuesURL, nil)
	if err != nil {
		return nil, err
	}

	// Add the private token to authenticate the request
	req.Header.Add("PRIVATE-TOKEN", privateToken)

	// Send the request
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Read the response body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// Check for HTTP status code indicating success
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status: %s", resp.Status)
	}

	// turn the response body into a slice of issues
	var issues []GitlabIssue
	err = json.Unmarshal(body, &issues)
	if err != nil {
		return nil, err
	}

	return issues, nil

}
